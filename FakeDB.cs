using System;
using System.Collections.Generic;

namespace ExemploMVC
{
    class FakeDB
    {
        private List<Models.Produto> Produtos { get; set; }
        public FakeDB()
        {
            this.Produtos = new List<Models.Produto>();
            Random r = new Random();
            for (int i = 1; i <= 100; i++)
            {
                this.Produtos.Add(new Models.Produto() {
                    Id = i,
                    Nome = "Produto " + i,
                    Valor = r.Next(100, 250),
                    Quantidade = r.Next(10,50)
                });
            }
        }

        public List<Models.Produto> ObterProdutos()
        {
            return this.Produtos;
        }
    }
}