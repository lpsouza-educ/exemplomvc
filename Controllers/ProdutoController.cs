using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace ExemploMVC.Controllers
{
    public class ProdutoController : Controller
    {
        public IActionResult Index()
        {
            FakeDB db = new FakeDB();

            List<Models.Produto> produtos = db.ObterProdutos();

            return View(produtos);
        }

        public IActionResult Unidade(int id)
        {
            FakeDB db = new FakeDB();

            List<Models.Produto> produtos = db.ObterProdutos();

            Models.Produto produto = produtos.Where(a => a.Id == id).FirstOrDefault();

            return View();
        }
    }
}